# spider-web

# Why

Automate netstat recording in defined intervals for analyzing machines

# What

It is a simple command-line application saving netstat output

# Usage

# Install 
## Production
The program can run on Windows and Linux. For the instructions see below. 

```bash
pip install spider-web
```
## Dev


```bash
git clone https://codeberg.org/cap_jmk/spider-web.git
```
## Linux

```bash 
bash install.sh
spider-web -o out_dir/
```


## Windows

Double click on `install.bat`. 

```bash
spider-web -o out_dir/
```

# Known problems

Weird behavior in Windows may intercept with reliable recording of the netstat command. However, it is better than nothing. Also, recording below the minute timescale seems challenging.
