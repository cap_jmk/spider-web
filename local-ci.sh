# Copyright Julian M. Kleber 

echo "local-ci  Copyright (C) 2022 Julian M. Kleber This program comes with ABSOLUTELY NO WARRANTY; for details type 'show w'. This is free software, and you are welcome to redistribute it under certain conditions; type 'show c' for details."

black spider_web/
find . -type f -name '*.txt' -exec sed --in-place 's/[[:space:]]\+$//' {} \+ #sanitize trailing whitespace
autopep8 --in-place --recursive spider_web/
python -m flake8 spider_web/ --count --select=E9,F63,F7,F82 --show-source --statistics
mypy --strict spider_web/
python -m pylint -f parseable spider_web/
pytest tests/